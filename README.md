# Evolution Application v1.0
Newly refactored Evolution API + More

## Requirements/Dependencies
- Node 11.4.0
- Running MongoDB Server

## Installation
1. **Clone Repository from GitSwarm**  
```
git clone git@bitbucket.org:vinlockz/evolution-server.git
```

2. **Install Node Dependencies**  
```
yarn install // Yarn (Recommended)  
// OR  
npm install // NPM  
```

3. **Build**  
```
yarn build // Yarn (Recommended)  
// OR  
npm run build // NPM  
```

4. **Run Application**  
Make sure that the require environment variables are set before attempting to start the application.
If you are using `nodemon` to rebuild on changes, you will still need to restart the app manually.
We are working on an auto app restart solution still as `nodemon` has issues with the application.  
```
yarn start // Yarn (Recommended)  
// OR  
npm start // NPM  
```

## Environment Variables
Environment Variables that already have defaults do not need to be given on application boot. Only required environment variables and ones that you want to change are needed.

### Application
| Environment Variable | Possible Values / Type       | Required | Default Value | Description                                                                                                |
|----------------------|------------------------------|----------|---------------|------------------------------------------------------------------------------------------------------------|
| NODE_ENV             | `production` / `development` | X        |  | Node Environment                                                                                           |
|ADMIN_JWT_SECRET|String|X||JWT Secret for the Admin Panel Cookie|
| DEBUG_MODE           | `true`                       |         | `false`       | Places the application into debug mode, enabling more verbose logging and other debug features.            |
| APP_PORT             | Integer                      |          | `4000`        | Port the application runs on.                                                                              |
| ADMIN_PORT           | Integer                      |          | 7425          | Port the Admin Panel runs on.                                                                              |

### Express
| Environment Variable           | Possible Values / Type | Required | Default Value | Description                                                                                                |
|--------------------------------|------------------------|----------|---------------|------------------------------------------------------------------------------------------------------------|
| DISABLE_HELMET                 | `true`                 |          | `false`       | Disables the Helmet Security Middleware.                                                                   |
| DISABLE_HELMET_HSTS            | `true`                 |          | `false`       | Disables Helmet HSTS Security Middleware. See https://helmetjs.github.io/docs/hsts/.                       |
| DISABLE_HELMET_CACHE_CONTROL   | `true`                 |          | `false`       | Disables Helmet Cache Control Security Middleware. See https://helmetjs.github.io/docs/nocache/.           |
| DISABLE_HELMET_CROSS_DOMAIN    | `true`                 |          | `false`       | Disables Helmet Cross Domain Security Middleware. See https://helmetjs.github.io/docs/crossdomain/.        |
| DISABLE_HELMET_HIDE_POWERED_BY | `true`                 |          | `false`       | Disables Helmet Hide Powered By Security Middleware. See https://helmetjs.github.io/docs/hide-powered-by/. |
|CORS_USE_VALID_HOSTS|`false`||`true`|Disable the usage of Admin Setting supplied Valid Hosts for CORS.|

### Logging
| Environment Variable               | Possible Values / Type | Required | Default Value | Description                                                                                                                                                                                      |
|------------------------------------|------------------------|----------|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| LOGGING_CLOUDWATCH_LOG_GROUP_NAME  | String                 |  `LOGGING_CLOUDWATCH_ENABLE`        |               | AWS Cloudwatch Log Group Name. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/Working-with-log-groups-and-streams.html.  |
| LOGGING_CLOUDWATCH_LOG_STREAM_NAME | String                 |  `LOGGING_CLOUDWATCH_ENABLE`        |               | AWS Cloudwatch Log Stream Name. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/Working-with-log-groups-and-streams.html. |
|REDACT_DISABLE|`true`||`false`|Disable the redaction of sensitive information in logs.|
|REDACT_KEYS   |  |   |   | Comma-delimited list of keys to redact from Log Objects<br />**Example:** `password,confirmPassword`  |
|REDACT_REPLACE   | String  |   | `[REDACTED]`  |  String used when redacting keys. |

### MongoDB
| Environment Variable           | Possible Values / Type | Required | Default Value | Description                                                                                                                                                    |
|--------------------------------|------------------------|----------|---------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| MONGO_CONNECTION_STRING        | String                 | X        |               | **Mongo:** Connection String. See https://docs.mongodb.com/manual/reference/connection-string/.<br />**Use `${db}` in place of where the database name will go.** |
| MONGO_DATABASE_NAME            | String                 | X        |               | **Mongo:** Primary Database Name                                                                                                                               |
| MONGO_ADMIN_DATABASE_NAME      | String                 | X        |               | **Mongo:** Admin Database Name                                                                                                                                 |

### Redis
| Environment Variable | Possible Values / Type | Required | Default Value | Description                 |
|----------------------|------------------------|----------|---------------|-----------------------------|
| REDIS_ENABLED        | `true`                 |          | `false`       | Enable the Redis Connection |
| REDIS_HOST           | String                 | `REDIS_ENABLED`         |               | Redis Hostname or IP        |
| REDIS_PORT   | Integer  |   | `6379`  | Redis Port  |
| REDIS_CLUSTER        | `true`                 |          | `false`       | Is Redis Cluster?           |
| REDIS_ENABLE_SSL     | `true`                 |          | `false`       | Enable Redis SSL?           |

### GraphQL
| Environment Variable         | Possible Values / Type | Required | Default Value | Description                               |
|------------------------------|------------------------|----------|---------------|-------------------------------------------|
| GRAPHQL_DISABLE              | `true`                 |          | `false`       | Disable GraphQL                           |
| GRAPHQL_ENABLE_GRAPHIQL      | `true`                 |          | `false`       | **GraphQL:** Enable GraphiQL Interface    |
| GRAPHQL_ENABLE_INTROSPECTION | `true`                 |          | `false`       | **GraphQL:** Enable GraphQL Introspection |

### Additional Environment Variables
| Environment Variable         | Possible Values / Type | Required | Default Value | Description                               |
|------------------------------|------------------------|----------|---------------|-------------------------------------------|
|AWS_REGION|String||`us-west-2`|AWS Region that the application is hosted in.|

## Creating a Mutation
This will walk you through creating a mutation on Evolution.

The file `graphql/lib/Mutation.js` was created to abstract the application flow of creating mutations within Evolution. All of Evolution's mutations are apart of a flat object, meaning they are not nested by any sort of organization. This required you to name your mutations very descriptively.

### Mutation Naming Convention
Evolution  uses one of the known best practices for naming it's mutations.
```
<Action><Type><Mutation|Input|Response|Error|ErrorCodes>
```
**Example:** CreateAccountInput
**Action:** Create  
**Type:** Account    

### Using `createMutation`

Defaults are listed for `Boolean` values.  
If invalid options are passed to `createMutation` then `INVALID_MUTATION_CONFIG` Error will be thrown on application start-up.

```javascript
export default createMutation({
  // Base Mutation Name
  name: 'CreateAccount',

  // Mutation Description. Required.
  desc: 'Create a new Account',

  // Require Authentication?
  requireAuth: false,

  // Error Options
  error: {
    // Error Codes
    codes: [
      { code: 'BAD_PASSWORD', desc: 'Bad Password' },
      ...
    ],

    // Include Common Codes from graphql/schema/mutations/utils.js
    includeCommonCodes: true,
  },

  // Input Options
  input: {
    // Response Description. Optional.
    desc: '',

    // Input Fields
    // See https://graphql.org/graphql-js/type/#graphqlinputobjecttype
    // Type: GraphQLInputObjectFieldConfig
    fields: { ... },

    // Set if Input Object is Nullable
    isNullable: false,
  },

  // Response Options
  response: {
    // Response Description. Optional.
    desc: '',

    // Response Fields
    // See https://graphql.org/graphql-js/type/#graphqlobjecttype
    // Type: GraphQLFieldConfig
    fields: { ... },
  },
  resolve: async (parentValue, args, context, info) => { ... },
});
```

#### GrahpQLFieldConfig
```
type GraphQLFieldConfig = {
  type: GraphQLOutputType;
  args?: GraphQLFieldConfigArgumentMap;
  resolve?: GraphQLFieldResolveFn;
  deprecationReason?: string;
  description?: ?string;
}
```

#### GraphQLInputObjectFieldConfig
```
type GraphQLInputObjectFieldConfig = {
  type: GraphQLInputType;
  defaultValue?: any;
  description?: ?string;
}
```
