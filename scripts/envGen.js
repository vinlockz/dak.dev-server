const AWS = require('aws-sdk');
const fs = require('fs');

const { DEPLOYMENT_GROUP_NAME } = process.env;

const secretsmanager = new AWS.SecretsManager({
  apiVersion: '2017-10-17',
  region: 'us-west-2',
});

const secretsLib = {
  production: 'prod/dak.dev',
};

const params = {
  SecretId: secretsLib[ DEPLOYMENT_GROUP_NAME ],
};

secretsmanager.getSecretValue(params, function(err, data) {
  if (err) console.error(err, err.stack); // an error occurred
  fs.writeFileSync(`${__dirname}/../pm2.json`, data.SecretString);
});
