/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
  GraphQLInt,
} from 'graphql';
import moment from 'moment';
import TimeMeasurement from '../enums/TimeMeasurement';

const DEFAULT_FORMAT = 'YYYY-MM-DDTHH:mm:ss';

const GraphQLMoment = new GraphQLObjectType({
  name: 'GraphQLMoment',
  description: 'GraphQL Moment Extension',
  fields: {
    format: {
      type: GraphQLNonNull(GraphQLString),
      description: 'Format Function\nhttps://momentjs.com/docs/#/displaying/format/',
      args: {
        format: {
          type: GraphQLString,
          description: 'Output Date Format',
          defaultValue: DEFAULT_FORMAT,
        },
      },
      resolve: (parentValue, args) => {
        const { date, fromFormat } = parentValue;
        const { format } = args;
        return moment(date, fromFormat).format(format);
      },
    },
    timeFromNow: {
      type: GraphQLNonNull(GraphQLString),
      description: 'Time from now\nhttps://momentjs.com/docs/#/displaying/fromnow/',
      args: {
        withoutSuffix: {
          type: GraphQLBoolean,
          defaultValue: false,
        },
      },
      resolve: (parentValue, args) => {
        const { date, fromFormat } = parentValue;
        const { withoutSuffix } = args;
        return moment(date, fromFormat).fromNow(withoutSuffix);
      },
    },
    timeFromX: {
      type: GraphQLNonNull(GraphQLString),
      description: 'Time from X\nhttps://momentjs.com/docs/#/displaying/from/',
      args: {
        X: {
          type: GraphQLString,
          defaultValue: 'NOW',
        },
        withoutSuffix: {
          type: GraphQLBoolean,
          defaultValue: false,
        },
      },
      resolve: (parentValue, args) => {
        const { date, fromFormat } = parentValue;
        const { withoutSuffix } = args;
        let { X } = args;
        if (X === 'NOW') X = moment();
        return moment(date, fromFormat).from(X, withoutSuffix);
      },
    },
    difference: {
      type: GraphQLNonNull(GraphQLInt),
      description: 'Difference\nhttps://momentjs.com/docs/#/displaying/difference/',
      args: {
        from: {
          type: GraphQLString,
          defaultValue: 'NOW',
        },
        measurement: {
          type: TimeMeasurement,
          defaultValue: 'DAYS',
        },
        float: {
          type: GraphQLBoolean,
          defaultValue: false,
        },
      },
      resolve: (parentValue, args) => {
        const { date, fromFormat } = parentValue;
        const { measurement, float } = args;
        let { from } = args;
        if (from === 'NOW') from = moment();
        return moment(date, fromFormat).diff(from, measurement, float);
      },
    },
    unixTimestamp: {
      type: GraphQLNonNull(GraphQLInt),
      description: 'UNIX Timestamp (Seconds)\nhttps://momentjs.com/docs/#/displaying/unix-timestamp/',
      args: {
        milliseconds: {
          type: GraphQLBoolean,
          defaultValue: false,
        },
      },
      resolve: (parentValue) => {
        const { date, fromFormat } = parentValue;
        const { milliseconds } = args;
        const dateInstance = moment(date, fromFormat);
        if (milliseconds) {
          return dateInstance.valueOf();
        }
        return dateInstance.unix();
      },
    },
  },
});

export default GraphQLMoment;
