/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import express from 'express';
import controllers from './controllers';
import middleware from '../../lib/ExpressMiddleware';

const { ADMIN_KEY } = process.env;

/** API Router **/
const apiRouter = express.Router();

/** MIDDLEWARE **/
apiRouter.use(middleware({
  jsonParser: true,
  requestIdGenerator: true,
  logging: { name: 'adminApi' },
}));

apiRouter.post('/auth', controllers.authController.auth);

/** Settings Router **/
const settingsRouter = express.Router();

// Require User or ADMIN API KEY
settingsRouter.use((req, res, next) => {
  if (req.user || req.headers['x-admin-key'] === ADMIN_KEY) {
    return next();
  } else {
    return res.status(403).json({
      error: 'ACCESS_DENIED',
    });
  }
});
settingsRouter.get('/:key', controllers.settingsController.get);
settingsRouter.post('/:key', controllers.settingsController.set);
apiRouter.use('/settings', settingsRouter);

export default apiRouter;
