/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import axios from 'axios';
import applyLoggingInterceptor from '../../../lib/AxiosInterceptors/AxiosInterceptors';
import jwt from 'jsonwebtoken';

/** ENVIRONMENT VARIABLES **/
const { INTERNAL_REST_API_ENDPOINT, ADMIN_JWT_SECRET } = process.env;

export default {
  auth: async (req, res) => {
    const { username, password, domain } = req.body;

    try {
      const internalRestApi = axios.create({
        baseURL: INTERNAL_REST_API_ENDPOINT,
        timeout: 20000,
        headers: {
          'Content-Type': 'application/json',
        },
      });

      applyLoggingInterceptor(internalRestApi, req.logger, 'internal-rest-api');

      const response = await internalRestApi.post('/ldap/auth', {
        domain: domain,
        data: Buffer.from(JSON.stringify({
          username, password
        })).toString('base64'),
      });
      const { data } = response;
      const doc = await req.db.User.findOneAndUpdate({
        email: data.mail,
      }, {
        email: data.mail,
        name: data.displayName,
        description: data.description,
      }, { upsert: true, new: true }).exec();
      const { name, description } = doc;
      return jwt.sign({
        _id: doc._id,
      }, ADMIN_JWT_SECRET, function deliverToken(err, token) {
        return res.json({
          name, description, token,
        });
      });
    } catch (err) {
      if (err.response) {
        return res.status(500).json({
          error: err.response.data.errorDesc,
        });
      } else {
        return res.status(500).json({
          error: err.message,
        });
      }
    }
  },
};