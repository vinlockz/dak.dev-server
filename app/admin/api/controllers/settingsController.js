/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

export default {
  get: async (req, res) => {
    const { key } = req.params;
    try {
      const settingValue = await req.db.Setting.get(key);
      if (settingValue) {
        return res.json(settingValue);
      } else {
        return res.status(404).json({ error: 'NOT_FOUND' });
      }
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  },
  set: async (req, res) => {
    const { key } = req.params;
    const { value } = req.body;
    try {
      await req.db.Setting.set(key, value).exec();
      return res.status(200).json({ success: true });
    } catch (err) {
      return res.status(500).json({
        success: false,
        error: err.message || null,
      });
    }
  },
};
