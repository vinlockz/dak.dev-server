/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React from 'react';
import Head from 'next/head';
import api from '../lib/api';
import { withCookies } from 'react-cookie';

// TODO: redo the login

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emailUsername: '',
      emailDomain: `@${emailDomains[0]}`,
      domain: domains[0],
      password: '',
      error: 0,
    };
  }

  static async getInitialProps(ctx) {
    if (!process.browser) {
      const { req, res } = ctx;
      if (req.user) {
        res.writeHead(302, { Location: '/' });
        res.end();
      }
    }
    return {};
  }

  login = (event) => {
    event.preventDefault();
    const { emailUsername, emailDomain, password, domain } = this.state;
    const { cookies } = this.props;
    api.post('/auth', {
      username: `${emailUsername}${emailDomain}`,
      password,
      domain,
    }).then(response => response.data)
      .then((data) => {
        console.log('data', data);
        cookies.set('token', data.token);
        location.reload();
      })
      .catch((err) => {
        this.setState(state => ({
          error: state.error + 1,
        }));
      });
    return false;
  };

  handleChangeField = (field) => (event) => {
    this.setState({
      [field]: event.target.value,
    });
  };

  handleEmailDomainChange = (value) => (event) => {
    this.setState({
      emailDomain: `@${value}`,
    });
  };

  handleDomainChange = (value) => (event) => {
    this.setState({
      domain: value,
    });
  };

  removeError = () => {
    this.setState({
      error: false,
    });
  };

  errorMessage = () => {
    const { error } = this.state;

    const errorCount = error === 0 || error === 1 ? '' : `(${error})`;

    if (error) {
      return (
        <div className="alert alert-danger" role="alert">
          Invalid Credentials, please try again! {errorCount}
        </div>
      );
    }
    return null;
  };

  render() {
    const { emailDomain, emailUsername, domain, password } = this.state;
    return (
      <div>
        <Head>
          <title>Login - ZephyrQL Admin</title>
        </Head>
        <div className="border shadow p-3 mb-5 bg-white rounded container text-center">
          <form className="col-lg-6 offset-lg-3 col-md-12" onSubmit={this.login}>
            <h2>Login</h2>
            {this.errorMessage()}
            <div className="form-group text-left">
              <label htmlFor="emailDomain">LDAP Domain</label>
              <div className="dropdown">
                <button className="btn btn-outline-secondary dropdown-toggle col-md-12" type="button" id="domainDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {domain}
                </button>
                <div className="dropdown-menu col-md-12" aria-labelledby="domainDropdown">
                  {domains.map((domain, key) => (
                    <button key={key} type="button" className="dropdown-item">{domain}</button>
                  ))}
                </div>
              </div>
            </div>
            <div className="form-group text-left">
              <label htmlFor="emailUsername">E-Mail Address</label>
              <div className="input-group">
                <input value={emailUsername} tabIndex={1} onChange={this.handleChangeField('emailUsername')} type="text" className="form-control text-left" aria-label="E-Mail Address" id="emailUsername" />
                <div className="input-group-append">
                  <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {emailDomain}
                  </button>
                  <div className="dropdown-menu">
                    {emailDomains.map((domain, key) => (
                      <button key={key} type="button" className="dropdown-item" onClick={this.handleEmailDomainChange(domain)}>
                        {`@${domain}`}
                      </button>
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group text-left">
              <label htmlFor="accountPassword">Password</label>
              <input value={password} onChange={this.handleChangeField('password')} type="password" id="accountPassword" tabIndex={2} className="form-control" />
            </div>
            <div className="d-flex flex-row-reverse">
              <button type="submit" className="btn btn-primary ">Login</button>
            </div>
          </form>
        </div>
      </div>
    );
  };
}

export default withCookies(Login);
