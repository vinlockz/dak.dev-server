/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import './NavHeader.css';

class NavHeader extends Component {
  constructor(props) {
    super(props);
    let identifier = null;
    if (props.user) {
      identifier = `Hello, ${props.user.name}`;
    }
    this.state = { identifier };
  }

  handleIdentifierClick = (event) => {
    const { email, name } = this.props.user;
    const { identifier } = this.state;
    if (email === identifier) {
      this.setState({ identifier: `Hello, ${name}` });
    } else {
      this.setState({ identifier: email });
    }
  };

  logOut = () => {
    const { cookies } = this.props;
    cookies.remove('token');
    location.reload();
  };

  loggedInNav = () => {
    const { user } = this.props;
    const { identifier } = this.state;
    if (user) {
      return (
        <div className="navbar-nav">
          <span className="navbar-text mr-3 custom-identifier" onClick={this.handleIdentifierClick}>{identifier}</span>
          <button
            onClick={this.logOut}
            className="nav-item nav-link btn"
            data-toggle="tooltip" data-placement="bottom" title="Logout"
          >
            <i className="fas fa-sign-out" />
          </button>
        </div>
      );
    }
    return (
      <div className="navbar-nav">
        <a className="nav-item nav-link" href="/login">Login</a>
      </div>
    );
  };

  nav = () => {
    const { user } = this.props;

    const items = [
      (
        <li key={0} className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
             aria-haspopup="true" aria-expanded="false">
            Global Settings
          </a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a className="dropdown-item" href="/globalSettings/validHosts">Valid Hosts</a>
            <a className="dropdown-item" href="/globalSettings/csCodes">CS Codes</a>
          </div>
        </li>
      ),
    ];

    return (
      <div className="navbar-nav mr-auto">
        {user ? items : null}
      </div>
    );
  };

  render() {
    return (
      <nav className="navbar navbar-dark bg-ultimate-blue navbar-expand-lg mt-2 mb-2">
        <div className="container">
          <a className="navbar-brand mb-0 h1" href="/">{process.env.ADMIN_TITLE ? process.env.ADMIN_TITLE : 'EvolutionCMS'}</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                  aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            {this.nav()}
            {this.loggedInNav()}
          </div>
        </div>
      </nav>
    );
  }
}

export default withCookies(NavHeader);
