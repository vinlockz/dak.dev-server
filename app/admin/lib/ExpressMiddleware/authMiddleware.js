/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import jwt from 'jsonwebtoken';
import db from '../../db';

const { ADMIN_JWT_SECRET } = process.env;

/** Admin User Middleware **/
const adminAuthMiddleware = () => async (req, res, next) => {
  req.loggedIn = false;

  const { cookies } = req;

  if (cookies.token) {
    let decoded = null;
    try {
      decoded = jwt.verify(cookies.token, ADMIN_JWT_SECRET);
    } catch (err) {
      return next(err);
    }
    if (decoded !== null) {
      const { _id } = decoded;
      try {
        req.user = await db.User.findOne({ _id }).exec();
        req.loggedIn = true;
      } catch (err) {
        return next(err);
      }
    }
  }

  // Redirect if logged in
  req.requireNoAuth = (redirect = '/') => {
    if (req.user) {
      res.writeHead(302, { Location: redirect });
      res.end();
    }
  };

  // Redirect if not logged in
  req.requireAuth = (redirect = '/login') => {
    if (!req.user) {
      res.writeHead(302, { Location: redirect });
      res.end();
    }
  };

  return next();
};

export default adminAuthMiddleware;
