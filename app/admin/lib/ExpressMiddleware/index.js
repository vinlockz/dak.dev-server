/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import adminDbConnector from './adminDbConnector';
import adminAuthMiddleware from './authMiddleware';
import auditLogMiddleware from './auditLogMiddleware';
import Validation from '../../../lib/Validation';

const adminMiddleware = (options = {}) => {
  const defaults = {
    adminDb: false,
    adminAuth: false,
    auditLog: false,
  };

  options = { ...defaults, ...options };

  const middleware = [];

  // Admin DB Connector
  if (options.adminDb) {
    if (Validation.isString(options.adminDb)) {
      middleware.push(adminDbConnector(options.adminDb));
    } else {
      middleware.push(adminDbConnector());
    }
  }

  // Admin Auth Middleware
  if (options.adminAuth) middleware.push(adminAuthMiddleware());

  // Audit Log Middleware
  if (options.auditLog) middleware.push(auditLogMiddleware());

  return middleware;
};

export default adminMiddleware;
