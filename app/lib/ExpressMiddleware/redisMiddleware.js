/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import Logger from '../Logger/Logger';
import Redis from 'ioredis';
import hash from 'object-hash';
import _ from 'lodash';

let client = null;

const redisCacheMiddleware = (options) => {
  const defaults = {
    redisEnabled: false,
    redisHost: null,
    redisPort: 6379,
    redisCluster: false,
    redisEnableSSL: false,
    redisAdditionalConfig: {},
  };

  options = _.extend(defaults, options);

  const logger = new Logger({ name: 'redis' });

  const middleware = [];

  logger.debug('redis.log', `Redis / Host: ${options.redisHost} | Port: ${options.redisPort}`);

  if (options.redisEnabled) {
    let config = {
      port: options.redisPort,
      host: options.redisHost,
      reconnectOnError: (err) => {
        if (err.message.startsWith('READONLY')) {
          logger.warn('redis.log', 'Redis server returned a READONLY error, reconnecting.');
        }
        return 2;
      },
      retryStrategy: (times) => {
        logger.warn('redis.log', 'Lost Redis connection, reattempting.');
        return Math.min(times * 2, 2000);
      },
      ...options.redisAdditionalConfig,
    };

    if (options.redisEnableSSL) {
      config.auth_pass = '';
      config.tls = {
        checkServerIdentity: () => undefined,
      };
    }

    if (options.redisCluster) {
      client = new Redis.Cluster([config], {
        scaleReads: 'slave',
      });
    } else {
      client = new Redis(config);
    }

    client.on('ready', () => {
      logger.log('redis.log', 'Redis is ready.');
    });

    client.on('error', (err) => {
      logger.error('redis.error', err);
    });

    const redisMiddleware = (req, res, next) => {
      if (client.status === 'ready') {
        req.redis = client;
        return next();
      } else {
        client.on('ready', () => {
          req.logger.debug('redis.log', 'Redis is ready.');
          req.redis = client;
          return next();
        });
      }
    };

    redisMiddleware.client = client;

    middleware.push(redisMiddleware);
  }

  middleware.push(function cacheMiddleware(req, res, next) {
    req.cache = async (key, fallback, expire = 10, isAsync = true) => {
      if (!options.redisEnabled && !Object.prototype.hasOwnProperty.call(req, 'redis')) {
        // Fallback since there is no redis.
        let response = null;
        if (isAsync) {
          response = await fallback();
        } else {
          response = fallback();
        }
        return response;
      }

      // If redis exists
      if (typeof key !== 'string' && typeof key === 'object') {
        key = hash(key);
      } else if (typeof key !== 'string' && typeof key !== 'object') {
        throw new TypeError('Invalid cache key type. Must be a string or an object.');
      }

      // Get cached response
      let response = await req.redis.get(key);

      // Log cached response
      req.logger.log('cache.getCache', { key, response });
      if (response === null) {
        // If there is no cached response, fallback and then set the new cached response.
        if (isAsync) {
          response = await fallback();
        } else {
          response = fallback();
        }

        // Asynchronously set the cache and log, do not need to await for it.
        req.logger.log('cache.setCache', { key, response });

        const stringifiedResponse = (typeof response === 'object') ? JSON.stringify(response) : response;

        req.redis.set(key, stringifiedResponse, 'EX', expire);
      } else {
        // Parse the JSON cache grab
        response = JSON.parse(response);
      }
      return response;
    };
    return next();
  });

  return middleware;
};

export default redisCacheMiddleware;