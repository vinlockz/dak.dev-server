/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import cors from 'cors';
import { URL } from 'url';

const {
  CORS_USE_VALID_HOSTS,
  CORS_IGNORE_PORT,
  CORS_ALLOW_LOCALHOST,
} = process.env;

const corsMiddleware = () => cors(async (req, callback) => {
  const corsOptions = {
    origin: false,
    optionsSuccessStatus: 200,
  };
  const validHosts = await req.settings.getValidHosts();
  const host = req.get('host');
  let origin = req.get('origin');
  const originUrl = new URL(origin);

  // Can ignore port?
  if (CORS_IGNORE_PORT === 'true') origin = originUrl.hostname;

  // Can allow localhost?
  if (CORS_ALLOW_LOCALHOST === 'true' && originUrl.hostname === 'localhost') {
    corsOptions.origin = true;
  }

  // Allow if API Host and Client Origin match.
  if (host === originUrl.host) corsOptions.origin = true;

  // If valid hosts is disabled then allow the request
  if (CORS_USE_VALID_HOSTS !== 'true') corsOptions.origin = true;

  // If valid hosts is enabled, check if the origin matches a valid host.
  if (Array.isArray(validHosts) && validHosts.some(host => (origin.endsWith(`.${host}`) || host === origin.hostname))) {
    corsOptions.origin = true;
  }

  const error = corsOptions.origin ? null : new Error('INVALID_ORIGIN')
  callback(error, corsOptions);
});

export default corsMiddleware;
