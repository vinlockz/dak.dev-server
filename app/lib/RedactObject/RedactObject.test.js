/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import redact from './RedactObject';


describe('RedactObject', function() {
  test('should redact password keys', () => {
    const log = {
      password: 'dasfadsfadsf',
      checker: [
        {
          password: 'asdeqwqgcasd',
        },
        {
          password: 'adasdasdsa',
          nonPassword: 'adasdasdas',
        },
      ],
      passwords: {
        password: 'fadsfadsf',
      },
      itall: {
        password: 'asdasdasd',
      },
      go: {
        password: {
          password: [
            'go',
          ],
        },
      },
    };

    return expect(redact(log, { keys: [ 'password' ] })).toEqual({
      password: '[REDACTED]',
      checker: [
        {
          password: '[REDACTED]',
        },
        {
          password: '[REDACTED]',
          nonPassword: 'adasdasdas',
        },
      ],
      passwords: {
        password: '[REDACTED]',
      },
      itall: {
        password: '[REDACTED]',
      },
      go: {
        password: '[REDACTED]',
      },
    });
  });
});
