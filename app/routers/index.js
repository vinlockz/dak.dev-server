/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import testRouter from './testRouter';
import graphQLRouter from './graphqlRouter';
import restRouter from './restRouter';
import notFoundRouter from './notFoundRouter';

const { GRAPHQL_DISABLE } = process.env;

export default (app) => {
  // Test Route
  app.use('/test', testRouter);

  // GraphQL
  if (GRAPHQL_DISABLE !== 'true') {
    app.use('/gql', graphQLRouter);
  }

  // Rest API
  if (REST_API_DISABLE !== 'true') {
    app.use('/rest', restRouter);
  }

  // 404 Not Found
  app.use(notFoundRouter);

  // On Error
  app.use((err, req, res) => res.status(500).json({
    code: err.code || null,
    message: err.message || null,
  }));
};
