/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

// import path from 'path';
import { Router } from 'express';
import middleware from '../lib/ExpressMiddleware';

const notFoundRouter = Router();

// Tracks and Logs Not Found Requests
notFoundRouter.use(middleware({
  requestIdGenerator: true,
  logging: true,
}));

// // Get Requests depend on accepts header.
// notFoundRouter.get('*', (req, res) => {
//   res.status(404);
//
//   if (req.accepts('html')) {
//     return res.sendFile(path.join(`${__dirname}/views/404.html`));
//   }
//
//   if (req.accepts('json')) {
//     return res.json({ error: 'NOT_FOUND' });
//   }
//
//   return res.type('txt').send('404 Not Found');
// });

notFoundRouter.use(
  middleware({ settings: true }),
  (req, res) => res.status(404).json({ error: 'NOT_FOUND' }),
);

export default notFoundRouter;
