/*
 * MIT License
 *
 * Copyright (c) 2019 Dak Washbrook
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import NoIntrospection from 'graphql-disable-introspection';
import { Router } from 'express';
import graphqlHTTP from 'express-graphql';
import graphqlPlayground from 'graphql-playground-middleware-express';
import { dataloaders, schema } from '../graphql';
import middleware from '../lib/ExpressMiddleware';

const {
  GRAPHQL_ENABLE_INTROSPECTION,
  GRAPHQL_ENABLE_GRAPHIQL,
  GRAPHIQL_POLLING_INTERVAL,
} = process.env;

const graphQLRouter = Router();

graphQLRouter.options('*', middleware({
  settings: true,
  cors: true,
  logging: 'graphql',
  requestIdGenerator: true,
  adminDb: true,
}));

// Establish GraphQL Validation Rules
const validationRules = [];
if (GRAPHQL_ENABLE_INTROSPECTION !== 'true') {
  validationRules.push(NoIntrospection);
}

// Attach GraphQL Express
graphQLRouter.post(
  '/',
  middleware({
    jsonParser: true,
    urlEncodedParser: true,
    requestIdGenerator: true,
    logging: 'graphql',
    redisCache: true,
    settings: true,
  }),
  graphqlHTTP(request => ({
    schema,
    formatError: (err) => {
      request.logger.log('graphql.error', err);
      return err;
    },
    graphiql: false,
    context: {
      request,
      dataloaders: dataloaders(request),
    },
    validationRules,
  })),
);

// GraphiQL Playground
if (GRAPHQL_ENABLE_GRAPHIQL === 'true') {
  graphQLRouter.get(
    '/',
    middleware({
      requestIdGenerator: true,
      logging: 'graphiql',
    }),
    graphqlPlayground({
      endpoint: '/gql',
      settings: {
        'schema.polling.interval': GRAPHIQL_POLLING_INTERVAL !== undefined ? Number(GRAPHIQL_POLLING_INTERVAL) : 1000,
      },
    }),
  );
}

// On Error
graphQLRouter.use((err, req, res, next) => res.status(500).json({
  code: err.code || null,
  message: err.message || null,
}));

export default graphQLRouter;
